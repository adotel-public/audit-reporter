<?php
/*
* This file is part of the Adotel application project
* (c) Adotel Company <hamid.udc@gmail.com> | 11/3/19
* For the full copyright and license information, please view the LICENSE
*/

namespace Adotel\Audit\Tests;

use Adotel\Audit\AuditObject;
use Adotel\Audit\Publisher;
use Adotel\Audit\ValueObject\Entity;
use Adotel\Audit\ValueObject\Metadata;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\TestCase;

/**
 * Description of UnitTest
 *
 * @author Hamid Seyedi <hamid.udc at gmail.com>
 */
final class UnitTest extends TestCase
{

    public function testInit()
    {
        $Publisher = Publisher::create('localhost', 'adotel', 'adotel', 5673, '/', __DIR__ . '/../temp');
        if (file_exists($Publisher->getFailedMessageTargetFile())) {
            unlink($Publisher->getFailedMessageTargetFile());
        }
        $this->assertInstanceOf(Publisher::class, $Publisher);
    }

    public function testFailedOnEmptyHost()
    {
        try {
            Publisher::create('', 'adotel', 'adotel', 5673, '/', __DIR__ . '/../temp');
        } catch (\Exception $e) {
            $Exception = $e;
        }
        $this->assertInstanceOf(\InvalidArgumentException::class, $Exception);
        $this->assertEquals($Exception->getMessage(), 'host is required');
    }

    public function testFailedOnEmptyUsername()
    {
        try {
            Publisher::create('localhost', '', 'adotel', 5673, '/', __DIR__ . '/../temp');
        } catch (\Exception $e) {
            $Exception = $e;
        }
        $this->assertInstanceOf(\InvalidArgumentException::class, $Exception);
        $this->assertEquals($Exception->getMessage(), 'username is required');
    }

    public function testFailedOnEmptyPassword()
    {
        try {
            Publisher::create('localhost', 'adotel', '', 5673, '/', __DIR__ . '/../temp');
        } catch (\Exception $e) {
            $Exception = $e;
        }
        $this->assertInstanceOf(\InvalidArgumentException::class, $Exception);
        $this->assertEquals($Exception->getMessage(), 'password is required');
    }

    public function testCreateEntityObject()
    {
        $this->assertInstanceOf(Entity::class, Entity::create(uniqid(), 'Entity', null));
    }

    public function testCreateMetadataObject()
    {
        $this->assertInstanceOf(Metadata::class,
            Metadata::create('3096', uniqid(), uniqid(), uniqid(), (new \DateTime())));

    }

    public function testCreatingAuditObjectPublish()
    {
        $this->assertInstanceOf(AuditObject::class, self::createAuditObject());
    }

    public function testPublish()
    {
        $Publisher = Publisher::create('rabbitmq', 'adotel', 'adotel', 5672, '/', __DIR__ . '/../temp');
        $Result    = $Publisher->save(self::createAuditObject());
        $this->assertEquals(true, $Result);
        sleep(1);
        $Publisher->getConnection()->reconnect();
        $Publisher->getConnection()->channel()->queue_purge('audit');
    }

    public function testSaveFailedMessages()
    {
        $Publisher  = Publisher::create('rabbitmq', 'adotel', 'adotel', 5673, '/', __DIR__ . '/../temp');
        $TargetFile = $Publisher->getFailedMessageTargetFile();
        try {
            $Publisher->save(self::createAuditObject(true));
        } catch (\Exception $e) {
        }
        try {
            $Publisher->save(self::createAuditObject(true));
        } catch (\Exception $e) {
        }
        try {
            $Publisher->save(self::createAuditObject());
        } catch (\Exception $e) {
        }
        try {
            $Publisher->save(self::createAuditObject());
        } catch (\Exception $e) {
        }
        $this->assertFileExists($TargetFile);
        $FileContent = trim(file_get_contents($TargetFile));
        $this->assertNotEmpty($FileContent);
        $Messages = explode("\n", $FileContent);
        $this->assertEquals(4, count($Messages));
    }

    public function testCron()
    {
        $Publisher = Publisher::create('rabbitmq', 'adotel', 'adotel', 5672, '/', __DIR__ . '/../temp');
        $Publisher->setConnection();
        $Publisher->retryFailedMessages();
        $Publisher->getConnection()->reconnect();
        $Channel = $Publisher->getConnection()->channel();
        /**
         * @var AMQPMessage $Message
         */
        $Message = $Channel->basic_get('audit', true);
        $this->assertEquals(4, $Message->delivery_info[ 'message_count' ] + 1);
    }

    private static function createAuditObject(bool $metadata = false)
    {
        return AuditObject::create(uniqid(),
            'http://',
            uniqid(),
            Entity::create(uniqid(), 'accommodation', null),
            null,
            null,
            Entity::create(uniqid(), 'user', AuditObject::ACTOR_TYPE_USER),
            200,
            $metadata !== false ? Metadata::create('306589', uniqid(), uniqid(), uniqid(), new \DateTime()) : null,
            AuditObject::EVENT_TYPE_CREATE,
            '{}',
            new \DateTime());
    }

    private function purgeQueue()
    {
        $Publisher = Publisher::create('rabbitmq', 'adotel', 'adotel', 5672, '/', __DIR__ . '/../temp');
        $Publisher->setConnection()->getConnection()->channel()->queue_purge('audit');
    }
}