<?php
/*
* This file is part of the Adotel application project
* (c) Adotel Company <hamid.udc@gmail.com> | 11/3/19
* For the full copyright and license information, please view the LICENSE
*/

require_once __DIR__ . '/../vendor/autoload.php';
