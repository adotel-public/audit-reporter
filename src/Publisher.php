<?php

/*
* This file is part of the Adotel application project
* (c) Adotel Company <hamid.udc@gmail.com> | 11/3/19
* For the full copyright and license information, please view the LICENSE
*/

namespace Adotel\Audit;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Webmozart\Assert\Assert;

/**
 * Description of Publisher
 *
 * @author Hamid Seyedi <hamid.udc at gmail.com>
 */
final class Publisher
{
    public const FAILED_MESSAGES_FILE = 'audit-failed-messages.data';

    /**
     * @var string
     */
    private $host;
    /**
     * @var int
     */
    private $port;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $vhost;
    /**
     * @var AMQPStreamConnection
     */
    private $connection;
    /**
     * @var string
     */
    private $tempDirectory;
    /**
     * @var string
     */
    private $tempFilename;

    /**
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $tempDirectory
     * @param string $tempFilename
     * @param int    $port
     * @param string $vhost
     *
     * @return Publisher
     */
    public static function create(
        string $host,
        string $username,
        string $password,
        int $port,
        string $vhost,
        string $tempDirectory,
        string $tempFilename = self::FAILED_MESSAGES_FILE
    ) {
        Assert::notEmpty($host, 'host is required');
        Assert::notEmpty($username, 'username is required');
        Assert::notEmpty($password, 'password is required');
        Assert::greaterThan($port, 0);
        Assert::notEmpty($tempFilename);
        Assert::directory($tempDirectory);

        $Instance                = new self();
        $Instance->host          = $host;
        $Instance->port          = $port;
        $Instance->username      = $username;
        $Instance->password      = $password;
        $Instance->vhost         = $vhost;
        $Instance->tempDirectory = $tempDirectory;
        $Instance->tempFilename  = $tempFilename;

        return $Instance;
    }

    /**
     * @param AuditObject $auditObject
     *
     * @return bool
     * @throws \Exception
     */
    public function save(AuditObject $auditObject)
    {
        $MessageBody = $auditObject->__toString();
        try {
            $this->setConnection();
            $this->retryFailedMessages();
            $this->sendMessageToQueue($MessageBody, $this->connection->channel());
        } catch (\Exception $e) {
            $this->saveForRetry($MessageBody);
        }

        if ($this->connection instanceof AMQPStreamConnection) {
            $this->connection->close();
        }
        if (isset($e)) {
            throw $e;
        }

        return true;
    }

    /**
     * @param string $message
     *
     * @return false|int
     */
    private function saveForRetry(string $message)
    {
        $TargetFile = $this->getFailedMessageTargetFile();
        if (!file_exists($TargetFile)) {
            touch($TargetFile);
            chmod($TargetFile, '644');
        }

        return file_put_contents($TargetFile, $message . "\n", FILE_APPEND);
    }

    /**
     * @return string
     */
    public function getFailedMessageTargetFile(): string
    {
        return $this->tempDirectory . DIRECTORY_SEPARATOR . $this->tempFilename;
    }

    /**
     * @throws \Exception
     */
    public function retryFailedMessages()
    {
        $TargetFile = $this->getFailedMessageTargetFile();
        if (!file_exists($TargetFile)) {
            return;
        }

        $FileContent = trim(file_get_contents($TargetFile));
        if (empty($FileContent)) {
            return;
        }

        $Messages = explode("\n", $FileContent);
        try {
            foreach ($Messages as $key => $message) {
                $this->sendMessageToQueue($message, $this->connection->channel());
                unset($Messages[ $key ]);
            }
        } catch (\Exception $e) {
            $Messages = array_values($Messages);
            file_put_contents($TargetFile, implode("\n", $Messages));
            throw $e;
        }

        unlink($this->getFailedMessageTargetFile());
    }

    /**
     * @param string      $message
     * @param AMQPChannel $channel
     */
    private function sendMessageToQueue(string $message, AMQPChannel $channel)
    {
        $AMQPMessage = new AMQPMessage($message, [
            'content_type'  => 'application/json',
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ]);

        $channel->queue_declare('audit', false, true, false, false);
        $channel->basic_publish($AMQPMessage, '', 'audit');
    }

    /**
     * @return AMQPStreamConnection
     */
    public function getConnection(): AMQPStreamConnection
    {
        return $this->connection;
    }

    /**
     * @return Publisher
     */
    public function setConnection(): Publisher
    {
        $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->username, $this->password);
        return $this;
    }
}