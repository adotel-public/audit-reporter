<?php
/*
* This file is part of the Adotel application project
* (c) Adotel Company <hamid.udc@gmail.com> | 11/4/19
* For the full copyright and license information, please view the LICENSE
*/

namespace Adotel\Audit;

use Adotel\Audit\ValueObject\Entity;
use Adotel\Audit\ValueObject\Metadata;
use Webmozart\Assert\Assert;

/**
 * Description of AuditObject
 *
 * @author Hamid Seyedi <hamid.udc at gmail.com>
 */
final class AuditObject
{

    public const ACTOR_TYPE_SYSTEM = 'SYSTEM';
    public const ACTOR_TYPE_USER = 'USER';
    public const EVENT_TYPE_CREATE = 'CREATE';
    public const EVENT_TYPE_UPDATE = 'UPDATE';
    public const EVENT_TYPE_DELETE = 'DELETE';

    /**
     * @var string
     */
    private $serviceId;
    /**
     * @var string
     */
    private $endpoint;
    /**
     * @var string
     */
    private $requestId;
    /**
     * @var Entity
     */
    private $aggregate;
    /**
     * @var string|null
     */
    private $traceabilityId;
    /**
     * @var string|null
     */
    private $tenantId;
    /**
     * @var Entity
     */
    private $actor;
    /**
     * @var int
     */
    private $statusCode = 0;
    /**
     * @var Metadata|null
     */
    private $metadata;
    /**
     * @var string
     */
    private $eventType;
    /**
     * @var string|null
     */
    private $eventPayload;
    /**
     * @var \DateTime
     */
    private $eventAt;

    /**
     * @param string        $serviceId
     * @param string        $endpoint
     * @param string        $requestId
     * @param Entity        $aggregate
     * @param string|null   $traceabilityId
     * @param string|null   $tenantId
     * @param Entity        $actor
     * @param int           $statusCode
     * @param Metadata|null $metadata
     * @param string        $eventType
     * @param string|null   $eventPayload
     * @param \DateTime     $eventAt
     *
     * @return AuditObject
     */
    public static function create(
        string $serviceId,
        string $endpoint,
        string $requestId,
        Entity $aggregate,
        ?string $traceabilityId,
        ?string $tenantId,
        Entity $actor,
        int $statusCode,
        ?Metadata $metadata,
        string $eventType,
        ?string $eventPayload,
        \DateTime $eventAt
    ) {

        foreach ([
                     'serviceId',
                     'endpoint',
                     'requestId',
                     'eventType',
                 ] as $prop) {
            Assert::stringNotEmpty($$prop, sprintf("%s is required", $prop));

        }
        Assert::oneOf($eventType, [self::EVENT_TYPE_CREATE, self::EVENT_TYPE_DELETE, self::EVENT_TYPE_UPDATE],
            'Invalid eventType');
        $Instance                 = new self();
        $Instance->serviceId      = $serviceId;
        $Instance->endpoint       = $endpoint;
        $Instance->requestId      = $requestId;
        $Instance->aggregate      = $aggregate;
        $Instance->traceabilityId = $traceabilityId;
        $Instance->tenantId       = $tenantId;
        $Instance->actor          = $actor;
        $Instance->statusCode     = $statusCode;
        $Instance->metadata       = $metadata;
        $Instance->eventType      = $eventType;
        $Instance->eventPayload   = $eventPayload;
        $Instance->eventAt        = $eventAt;
        return $Instance;
    }

    public function __toString(): string
    {
        $Array     = [];
        $Variables = get_object_vars($this);
        foreach ($Variables as $name => $value) {
            if ($value instanceof \DateTimeInterface) {
                $Array[ $name ] = $value->format('c');
                continue;
            }
            if ($value instanceof Entity || $value instanceof Metadata) {
                $Array[ $name ] = $value->toArray();
                continue;
            }
            $Array[ $name ] = $value;
        }
        return json_encode($Array);
    }

}