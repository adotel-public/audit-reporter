<?php
/*
* This file is part of the Adotel application project
* (c) Adotel Company <hamid.udc@gmail.com> | 12/22/19
* For the full copyright and license information, please view the LICENSE
*/

namespace Adotel\Audit\ValueObject;

use Webmozart\Assert\Assert;

/**
 * Description of Entity
 *
 * @author Hamid Seyedi <hamid.udc at gmail.com>
 */
final class Entity
{

    /**
     * @var string
     */
    private $id;
    /**
     * @var string|null
     */
    private $label;
    /**
     * @var string|null
     */
    private $type;

    /**
     * @param string      $id
     * @param string      $label
     * @param string|null $type
     *
     * @return Entity
     */
    public static function create(string $id, ?string $label, ?string $type)
    {
        Assert::stringNotEmpty($id, 'Target entity id is required.');
        Assert::nullOrRegex($type, '/^[a-zA-Z]*$/', sprintf("Invalid entity type given %s", $type));
        $Instance        = new self();
        $Instance->id    = $id;
        $Instance->label = $label;
        $Instance->type  = $type;
        return $Instance;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    public function toArray(): array
    {
        return ['id' => $this->id, 'label' => $this->label, 'type' => $this->type];
    }
}