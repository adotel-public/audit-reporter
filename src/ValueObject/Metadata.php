<?php
/*
* This file is part of the Adotel application project
* (c) Adotel Company <hamid.udc@gmail.com> | 12/22/19
* For the full copyright and license information, please view the LICENSE
*/

namespace Adotel\Audit\ValueObject;

use Webmozart\Assert\Assert;

/**
 * Description of Metadata
 *
 * @author Hamid Seyedi <hamid.udc at gmail.com>
 */
final class Metadata
{

    /**
     * @var string
     */
    private $propertyId;
    /**
     * @var string|null
     */
    private $channelId;
    /**
     * @var string
     */
    private $roomTypeId;
    /**
     * @var string|null
     */
    private $ratePlanId;
    /**
     * @var \DateTime
     */
    private $date;

    public static function create(
        string $propertyId,
        ?string $channelId,
        string $roomTypeId,
        ?string $ratePlanId,
        \DateTime $date
    ) {
        Assert::stringNotEmpty($propertyId, 'Metadata property ID is required.');
        Assert::stringNotEmpty($roomTypeId, 'Metadata room type ID is required.');
        $Instance             = new self();
        $Instance->propertyId = $propertyId;
        $Instance->channelId  = $channelId;
        $Instance->roomTypeId = $roomTypeId;
        $Instance->ratePlanId = $ratePlanId;
        $Instance->date       = $date;
        return $Instance;
    }

    /**
     * @return string
     */
    public function getPropertyId(): string
    {
        return $this->propertyId;
    }

    /**
     * @return string|null
     */
    public function getChannelId(): ?string
    {
        return $this->channelId;
    }

    /**
     * @return string
     */
    public function getRoomTypeId(): string
    {
        return $this->roomTypeId;
    }

    /**
     * @return string|null
     */
    public function getRatePlanId(): ?string
    {
        return $this->ratePlanId;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function toArray(): array
    {
        return [
            'propertyId' => $this->propertyId,
            'channelId'  => $this->channelId,
            'roomTypeId' => $this->roomTypeId,
            'ratePlanId' => $this->ratePlanId,
            'date'       => $this->date->format('c')
        ];
    }
}