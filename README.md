# Audit-reporter
A reusable package that connect to audit service

#Installation 
````
$ composer require adotel/audit-reporter
````

# Example
````
<?php


$Publisher = Adotel\Audit\Publishe::create('rabbitmq_host', 'rabbitmq_user', 'rabbitmq_pass');
$Params    = [
            'serviceId',
            'http://',
            'requestId',
            'aggregateId',
            'aggregateType',
            null, // traceabilityId
            null, // tenantId
            AuditObject::ACTOR_TYPE_USER,
            'actorId',
            200, // statusCode
            AuditObject::EVENT_TYPE_CREATE,
            '{payloads...}',
            new \DateTime()
        ];
$Result    = $Publisher->save(Adotel\Audit\AuditObject::create(...$Params));

```` 

# Cron
To push failed messages into the audit queue, Put following code in your service which cron will execute.
 
````
$Publisher = Adotel\Audit\Publishe::create('rabbitmq_host', 'rabbitmq_user', 'rabbitmq_pass');
$Publisher->cron();
````